<!-- implementation PDFOBJECT -->

//List of files
let FilesTab = ["mohamedyoussef_JEMAI_attestation_de_bonne_éxécution_AlberoIT", "Certificat de Participation JOBS 2019 Tunisia Mohamed Youssef JEMAI.pdf", "attestation_de_bonne_éxécution_hkeya.tn.pdf","Attestation de bonne execution_ Youssef jemai_ASL.pdf", "Certificat de participation facilitateur ISETCOM CONNECT 2.0 Mohamed Youssef JEMAI.pdf", "Certification Voltaire Mohamed Youssef JEMAI.pdf", "Certificat de Participation INJAZ Tunisia Mohamed Youssef JEMAI.pdf", "Certificat de Participation Formation Aflatoun Centre ENDA Mohamed Youssef JEMAI.pdf", "Attestation de bonne execution_ Youssef jemai_Roj.pdf", "Certificat de Participation Formation ANGULAR TEK UP Mohamed Youssef JEMAI.pdf", "RELEVE DE NOTES BAC 2016 Mohamed Youssef JEMAI.pdf", "Certificat d'organisation TDC Mohamed Youssef JEMAI.pdf", "Attestation Stage Assurance COMAR & HAYET Mohamed Youssef JEMAI.pdf", "Attestation Stage Ouvrier à Netcom Mohamed Youssef JEMAI.pdf", "Attestation Stage Technicien à Netcom Mohamed Youssef JEMAI.pdf", "Certificat de Participation Formation Gestion de stresse Rotaract ESPRIT Mohamed Youssef JEMAI.pdf", "RELEVE DE NOTES ISETCOM  Mohamed Youssef JEMAI.pdf", "diplome isetcom", "releve de note esprit 3eme"]
//Initialize file number
let i = null;

// intialize local icon
let ifConnected = window.navigator.onLine;

if (!ifConnected) {
    $(".print-file").css('display', 'none');
}
//  Image click open pop um
$(".img-fluid").on("click", function () {
    i = this.getAttribute("data-value");
    console.log("test")
    if (screen.width <= 768) {
        window.open('assets/documents/' + FilesTab[i]);

        $('#modal' + i).on('shown.bs.modal', function (e) {
            $('#modal' + i).modal('hide');

        })
    } else {

        $('#modal' + i).modal('show');

        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // need to pass in parameter id open for conditional modal
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        PDFObject.embed("assets/documents/" + FilesTab[i], "#pdf" + i

            , {
                pdfOpenParams: {
                    toolbar: '1',
                    navpanes: '0',
                    view: 'FitH'

                }
            });
    }
});

$('.onClose').on("click", function () {
    i = this.getAttribute("data-value");
    $("#modal" + i).modal('toggle');
    $(".modal-backdrop").remove();
});




